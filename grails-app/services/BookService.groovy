/**
 *
 *
 * @author biniam.asnake on 27/04/16.
 */
class BookService {

    List<Book> getAllBooks(Status status) {

        status.code = "Successful"
        status.message = "All data is read"

        if(!status.errorList)
            status.errorList = new ArrayList<String>()
        status.errorList.add("No errors")
        status.errorList.add("I said no errors")
        status.errorList.add("Im telling you again - no errors")
        status.save(flush: true)

        return Book.findAll()
    }
}
