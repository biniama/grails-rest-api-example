/**
 *
 *
 * @author biniam.asnake on 27/04/16.
 */
class Status {

    String code
    String message

    static hasMany = [errorList : String]

    static constraints = {

        code nullable: true, blank: true
        message nullable: true, blank: true
        errorList nullable: true
    }
}
