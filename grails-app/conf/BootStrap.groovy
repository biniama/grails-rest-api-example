class BootStrap {

    def init = { servletContext ->

        new Book(title: 'Book One', isbn: '1-ABC').save(flush: true)
        new Book(title: 'Book Two', isbn: '2-ABC').save(flush: true)
        new Book(title: 'Book Three', isbn: '3-ABC').save(flush: true)
    }

    def destroy = {
    }
}
