import grails.converters.JSON
import grails.rest.RestfulController
import grails.util.Holders
import javax.servlet.http.HttpServletResponse

class BookController extends RestfulController {

    static allowedMethods = [index: "GET"]

    BookService bookService

    def index() {

        Status status = new Status(code: 'Processing', message: 'Accepted request').save(flush: true)

        response.setStatus(HttpServletResponse.SC_ACCEPTED)
        response.setHeader('Location', "${Holders.applicationContext.grailsApplication.config.grails.serverURL}/status/${status.id}")
        render bookService.getAllBooks(status) as JSON
    }

    def show(Book bookInstance) {
        respond bookInstance
    }

    def create() {
        respond new Book(params)
    }
}
