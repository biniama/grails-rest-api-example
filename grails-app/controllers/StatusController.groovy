import grails.converters.JSON
import grails.rest.RestfulController
import grails.util.Holders

import javax.servlet.http.HttpServletResponse

class StatusController extends RestfulController {

    static allowedMethods = [index: "GET"]

    def index() {

        String id = params.id
        Status status = Status.get(id.toLong())
        status.code == "Successful"? response.setStatus(HttpServletResponse.SC_OK) :
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
        render status as JSON
    }

    def show(Book bookInstance) {
        respond bookInstance
    }

    def create() {
        respond new Book(params)
    }
}
